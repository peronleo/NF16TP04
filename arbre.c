//
// Created by Leo Peron on 21/11/2021.
//

#include <stdlib.h>
#include <string.h>
#include "stdio.h"
#include "arbre.h"
#include "pile.h"

Arbre* creerArbre(char* titre, T_Noeud* noeud) {
    Arbre* a = malloc(sizeof(Arbre));
    strcpy(a->nom, titre);
    a->arbre = noeud;
    return a;
}

T_Noeud* creerNoeud(char* mot) {

    T_Noeud* noeud = malloc(sizeof(T_Noeud));

    for (int i = 0; mot[i] != '\0'; i++) {
        if(mot[i] >= 'A' && mot[i] <= 'Z') mot[i] += 32;
        if(mot[i] == '\n') mot[i] = '\0';
    }

    noeud->mot = malloc(sizeof(mot) + 1);
    strcpy(noeud->mot, mot);

    noeud->nbr_occ=1;
    noeud->fils_droit= NULL;
    noeud->fils_gauche= NULL;

    return noeud;
}

T_arbre ajouterNoeud(char* mot, T_arbre arbre) {
    T_Noeud* noeud = arbre;
    T_Noeud* suivant = arbre;

    for (int i = 0; mot[i] != '\0'; i++) {
        if(mot[i] >= 'A' && mot[i] <= 'Z') mot[i] += 32;
        if(mot[i] == '\n') mot[i] = '\0';
    }

    while (suivant != NULL) {

        noeud = suivant;

        if(strcmp(noeud->mot, mot) > 0)
            suivant=suivant->fils_gauche;
        else if(strcmp(noeud->mot, mot) < 0)
            suivant=suivant->fils_droit;

        else {
            noeud->nbr_occ++;
            return arbre;
        }
    }

    if(strcmp(noeud->mot, mot) < 0)
        noeud->fils_droit = creerNoeud(mot);
    if(strcmp(noeud->mot, mot) > 0)
        noeud->fils_gauche = creerNoeud(mot);

    return arbre;
}

T_arbre retirerNoeud(char* mot, T_arbre arbre) {
    T_Noeud* ancien = arbre;
    T_Noeud* suivant = arbre;

    while (strcmp(suivant->mot, mot) != 0) {

        ancien = suivant;

        if(strcmp(suivant->mot, mot) > 0)
            suivant=suivant->fils_gauche;
        else if(strcmp(suivant->mot, mot) < 0)
            suivant=suivant->fils_droit;

        if(suivant == NULL) break;
    }

    if(suivant==NULL) {
        printf("Noeud non trouvé");
        return arbre;
    } else {
        suivant->nbr_occ--;
        if(suivant->nbr_occ==0) supprimerNoeud(suivant, ancien, arbre);
    }

    return arbre;
}

void supprimerNoeud(T_Noeud* supprimer, T_Noeud* ancien, T_arbre arbre) {

    // Cas où pas de fils, bout d'arbre
    if(supprimer->fils_gauche == NULL && supprimer->fils_droit == NULL) {
        // Cas où arbre avec feuille unique
        if(supprimer==arbre) cleanArbre(arbre);
        // Autre cas
        if(ancien->fils_droit == supprimer) ancien->fils_droit=NULL;
        else ancien->fils_gauche=NULL;
    // Cas où fils unique, on switch
    } else if(supprimer->fils_gauche == NULL || supprimer->fils_droit == NULL) {
        if(ancien->fils_droit == supprimer ) {
            if(supprimer->fils_droit !=NULL)  ancien->fils_droit=supprimer->fils_droit;
            else ancien->fils_droit=supprimer->fils_gauche;
        } else {
            if(supprimer->fils_droit !=NULL)  ancien->fils_gauche=supprimer->fils_droit;
            else ancien->fils_gauche=supprimer->fils_gauche;
        }
    // Cas où deux fils
    } else {
        T_Noeud* replace = Sucesseurs(supprimer->fils_droit);
        if(ancien->fils_droit == supprimer ) ancien->fils_droit = replace;
        else ancien->fils_gauche = replace;
    }

    cleanNoeud(supprimer);
}

void affichageArbre(T_arbre arbre) {
    Pile* p = creerPile();
    T_Noeud* tmp = arbre;
    T_Noeud* cmp = tmp;
    while (tmp != NULL || !estVide(p)) {
        if(tmp != NULL) {
            empiler(p, tmp);
            tmp = tmp->fils_gauche;
        } else {

            tmp = depiler(p);

            if(strncmp(tmp->mot, cmp->mot, 1) == 0 && tmp != arbre)
                printf("     %s [%d] \n", tmp->mot, tmp->nbr_occ);
            else
                printf("%c -- %s [%d] \n", tmp->mot[0]-32, tmp->mot, tmp->nbr_occ);

            cmp=tmp;

            tmp = tmp->fils_droit;
        }
    }
}

int arbreParfait(T_arbre arbre) {
    Pile* p = creerPile();
    int h=0;
    T_Noeud* tmp = arbre;
    while (tmp != NULL || !estVide(p)) {
        if(tmp != NULL) {
            empiler(p, tmp);
            tmp = tmp->fils_gauche;
        } else {

            tmp = depiler(p);

            if(tmp->fils_droit == NULL && tmp->fils_gauche == NULL)  {
                if(h == 0) h = hauteur(arbre, tmp);
                if(h != hauteur(arbre, tmp)) return 0;
            }

            tmp = tmp->fils_droit;

        }
    }
    return 1;
}

int arbreEquilibre(T_arbre arbre) {
    Pile* p = creerPile();
    T_Noeud* tmp = arbre;
    int max=0, min=0;
    while (tmp != NULL || !estVide(p)) {
        if(tmp != NULL) {

            if(tmp->fils_droit == NULL && tmp->fils_gauche == NULL) {

                if(min == 0) min = hauteur(arbre, tmp);
                if(hauteur(arbre, tmp) > max) max = hauteur(arbre, tmp);
                else if(hauteur(arbre, tmp) < min) min = hauteur(arbre, tmp);

                if(abs(max - min) > 1) return 0;
            }

            empiler(p, tmp);
            tmp = tmp->fils_gauche;
        } else {
            tmp = depiler(p);
            tmp = tmp->fils_droit;
        }
    }

    return 1;
}

T_Noeud* Sucesseurs(T_Noeud* noeud) {
    T_Noeud* act;
    act = noeud;
    while (act->fils_gauche != NULL && act != NULL) {
        act = act->fils_gauche;
    }
    return act;
}

int hauteur(T_arbre arbre, T_Noeud* noeud) {
    T_Noeud* suivant = arbre;
    int hauteur=0;

    while (suivant != NULL && suivant != noeud) {

        if(strcmp(suivant->mot, noeud->mot) > 0)
            suivant=suivant->fils_gauche;
        else if(strcmp(suivant->mot, noeud->mot) < 0)
            suivant=suivant->fils_droit;

        hauteur++;
    }

    if(suivant == NULL) return -1;

    return hauteur;
}

void cleanNoeud(T_Noeud* noeud) {
    free(noeud->mot);
    free(noeud);
}

void cleanArbre(T_arbre arbre) {
    int side;
    Pile* p = creerPile();
    T_Noeud* tmp = arbre;
    int max=0, min=0;
    while (tmp != NULL || !estVide(p)) {
        if(tmp != NULL) {

            //if(tmp->fils_droit == NULL && tmp->fils_gauche == NULL);
            //          d
            //      b       f
            //    a   c    e g
            empiler(p, tmp);
            tmp = tmp->fils_gauche;
        } else {
            if(tmp->fils_droit == NULL && tmp->fils_gauche == NULL) {
                cleanNoeud(tmp);
            }
            tmp = depiler(p);
            tmp = tmp->fils_droit;
        }
    }
}

void cleanArbres(Arbre* tab[50]) {
    for (int i = 0; i < 49; i++) {
        if(tab[i] == NULL) return;
        else cleanArbre(tab[i]->arbre);
    }
}