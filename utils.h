//
// Created by Leo Peron on 09/12/2021.
//

#include "arbre.h"

#ifndef TP04_UTILS_H
#define TP04_UTILS_H

int findSpace(Arbre* tab[50]);

void affichageAllArbres(Arbre* tab[50]);

void viderBuffer();

#endif //TP04_UTILS_H
