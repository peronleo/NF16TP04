//
// Created by Leo Peron on 09/12/2021.
//

#ifndef TP04_LISTE_H
#define TP04_LISTE_H

#include "arbre.h"

typedef struct Liste {
    char* mot;
    int nbr;
    struct Liste* suivant;
} Liste;

Liste* creerMot(T_Noeud* noeud);

Liste* ajouterListe(Liste* l, T_Noeud* noeud);

Liste* arbreTolexique(T_arbre arbre);

void supprimerListe(Liste* l);

void affichageListe(Liste* l);

float indiceJacard(Liste* l1, Liste* l2);

void cleanListes(Liste* tab[50]);

void cleanListe(Liste* l);

#endif //TP04_LISTE_H
