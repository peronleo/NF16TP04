#include <stdio.h>
#include "arbre.h"
#include "liste.h"
#include "utils.h"

int main() {

    char mot[50], mot2[50];
    int space, choixinter, choixinter2;
    Arbre* TABLEAU[50];
    Liste* TABLEAU2[50];
    // Reset memory
    for (int i = 0; i < 50; i++) {
        TABLEAU[i] = NULL;
        TABLEAU2[i] = NULL;
    }
    char choix = '0';

    while(choix != '9') {
        choixinter=choixinter2=-1;
        printf("--------------------- TP4 ---------------------\n");
        printf("1. Creer un arbre\n");
        printf("2. Afficher un arbre\n");
        printf("3. Ajouter un mot dans un arbre\n");
        printf("4. Retirer un mot dans un arbre\n");
        printf("5. Vérifier si un arbre est parfait\n");
        printf("6. Vérifier si un arbre est équilibré\n");
        printf("7. Transformer en lexique et afficher lexique\n");
        printf("8. Similarité des deux textes\n");
        printf("-----------------------------------------------\n");
        choix = getchar();
        viderBuffer();
        switch (choix) {
            // CREER UN ARBRE
            case '1':
                printf("Rentrez un nom pour votre arbre\n");
                scanf("%s", mot);
                viderBuffer();
                printf("Quel mot voulez-vous rajouter ?\n");
                scanf("%s", mot2);
                viderBuffer();
                space = findSpace(TABLEAU);
                if(space == -1) printf("Erreur: Trop de tableaux, veuillez en retirer un.\n");
                else {
                    TABLEAU[space] = creerArbre(mot, creerNoeud(mot2));
                }
                break;
            // AFFICHER ARBRE
            case '2':
                affichageAllArbres(TABLEAU);
                printf("Choisissez l'arbre:\n");
                scanf("%d", &choixinter);
                viderBuffer();
                if(TABLEAU[choixinter] == NULL) break;
                affichageArbre(TABLEAU[choixinter]->arbre);
                break;
            // AJOUTER UN MOT
            case '3':
                affichageAllArbres(TABLEAU);
                printf("Choisissez l'arbre:\n");
                scanf("%d", &choixinter);
                viderBuffer();
                if(TABLEAU[choixinter] == NULL) break;
                printf("Quel mot voulez-vous rajouter ?\n");
                scanf("%s", mot2);
                viderBuffer();
                TABLEAU[choixinter]->arbre = ajouterNoeud(mot2, TABLEAU[choixinter]->arbre);
                break;
            // RETIRER UN MOT
            case '4':
                affichageAllArbres(TABLEAU);
                printf("Choisissez l'arbre:\n");
                scanf("%d", &choixinter);
                viderBuffer();
                if(TABLEAU[choixinter] == NULL) break;
                printf("Quel mot voulez-vous retirer ?\n");
                scanf("%s", mot2);
                viderBuffer();
                TABLEAU[choixinter]->arbre = retirerNoeud(mot2, TABLEAU[choixinter]->arbre);
                break;
            // ARBRE PARFAIT
            case '5':
                affichageAllArbres(TABLEAU);
                printf("Choisissez l'arbre:\n");
                scanf("%d", &choixinter);
                viderBuffer();
                if(TABLEAU[choixinter] == NULL) break;
                viderBuffer();
                if(arbreParfait(TABLEAU[choixinter]->arbre) == 1) printf("L'Arbre est parfait.\n");
                else printf("L'arbre n'est pas parfait\n");
                break;
            // ARBRE EQUILIBRE
            case '6':
                affichageAllArbres(TABLEAU);
                printf("Choisissez l'arbre:\n");
                scanf("%d", &choixinter);
                viderBuffer();
                if(TABLEAU[choixinter] == NULL) break;
                viderBuffer();
                if(arbreEquilibre(TABLEAU[choixinter]->arbre) == 1) printf("L'Arbre est équilibré.\n");
                else printf("L'arbre n'est pas équilibré\n");
                break;
            // LISTE + AFFICHAGE
            case '7':
                affichageAllArbres(TABLEAU);
                printf("Choisissez l'arbre:\n");
                scanf("%d", &choixinter);
                viderBuffer();
                if(TABLEAU[choixinter] == NULL) break;
                viderBuffer();
                TABLEAU2[choixinter] = arbreTolexique(TABLEAU[choixinter]->arbre);
                printf("L'Arbre a bien été converti\n");
                affichageListe(TABLEAU2[choixinter]);
                break;
            // SIMILARITE
            case '8':
                affichageAllArbres(TABLEAU);
                printf("Choisissez l'arbre 1:\n");
                scanf("%d", &choixinter);
                viderBuffer();
                if(TABLEAU[choixinter] == NULL) break;
                viderBuffer();
                printf("Choisissez l'arbre 2:\n");
                scanf("%d", &choixinter2);
                if(TABLEAU[choixinter] == NULL) break;
                viderBuffer();
                // probleme de condition ici
                if(TABLEAU2[choixinter] == NULL) TABLEAU2[choixinter] = arbreTolexique(TABLEAU[choixinter]->arbre);
                if(TABLEAU2[choixinter2] == NULL) TABLEAU2[choixinter2] = arbreTolexique(TABLEAU[choixinter2]->arbre);
                // probleme de return et calcul du ratio
                printf("La similarité des deux textes est: %f\n", indiceJacard(TABLEAU2[choixinter], TABLEAU2[choixinter2]));
                break;
            // CLEAN RAM
            case '9':
                cleanArbres(TABLEAU);
                cleanListes(TABLEAU2);
                break;
        }
    }

    return 0;
}
