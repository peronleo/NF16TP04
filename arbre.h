//
// Created by Leo Peron on 21/11/2021.
//

#ifndef TP04_ARBRE_H
#define TP04_ARBRE_H

typedef struct Noeud {
    char *mot;
    int nbr_occ;
    struct Noeud* fils_gauche;
    struct Noeud* fils_droit;
} T_Noeud;

typedef T_Noeud* T_arbre;

typedef struct arbre{
    T_arbre arbre;
    char nom[50];
}Arbre;

Arbre* creerArbre(char* titre, T_Noeud* noeud);

T_Noeud* creerNoeud(char* mot);

T_arbre ajouterNoeud(char* mot, T_arbre arbre);

T_arbre retirerNoeud(char* mot, T_arbre arbre);

void supprimerNoeud(T_Noeud* supprimer, T_Noeud* ancien, T_arbre arbre);

void affichageArbre(T_arbre arbre);

int arbreParfait(T_arbre arbre);

int arbreEquilibre(T_arbre arbre);

void cleanNoeud(T_Noeud* noeud);

void cleanArbres(Arbre* tab[50]);

void cleanArbre(T_arbre arbre);

// UTILS

int hauteur(T_arbre arbre, T_Noeud* noeud);

T_Noeud* Sucesseurs(T_Noeud* noeud);

#endif //TP04_ARBRE_H
