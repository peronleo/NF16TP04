//
// Created by Leo Peron on 09/12/2021.
//

#include <stdlib.h>
#include "stdio.h"
#include <string.h>
#include "liste.h"
#include "pile.h"

Liste* creerMot(T_Noeud* noeud) {
    Liste* l = malloc(sizeof(Liste));
    l->mot= malloc(sizeof(noeud->mot) + 1);
    l->mot = noeud->mot;
    l->nbr = noeud->nbr_occ;
    l->suivant = NULL;
    return l;
}

Liste* ajouterListe(Liste* l, T_Noeud* noeud) {

    Liste* new = creerMot(noeud);
    Liste* act = l;
    Liste* suiv = act;

    // Boucle d'exploration
    while (suiv != NULL) {
        act=suiv;
        suiv=suiv->suivant;
    }

    act->suivant = new;

    return l;
}

Liste* arbreTolexique(T_arbre arbre) {
    Pile* p = creerPile();
    T_Noeud* tmp = arbre;
    Liste* l = NULL;
    while (tmp != NULL || !estVide(p)) {
        if(tmp != NULL) {
            empiler(p, tmp);
            tmp = tmp->fils_gauche;
        } else {

            tmp = depiler(p);
            if(l == NULL)
                l = creerMot(tmp);
            else
                l = ajouterListe(l, tmp);

            tmp = tmp->fils_droit;
        }
    }

    return l;
}

void affichageListe(Liste* l) {

    Liste* act = l;
    Liste* suiv = act;

    while (suiv != NULL) {

        if(strncmp(act->mot, suiv->mot, 1) == 0 && suiv != l)
            printf("     %s [%d]\n", suiv->mot, suiv->nbr);
       else
            printf("%c -- %s [%d] \n", suiv->mot[0]-32, suiv->mot, suiv->nbr);
        act = suiv;
        suiv=suiv->suivant;
    }
}

float indiceJacard(Liste* l1, Liste* l2) {

    float sim=0, tot=0;
    Liste* act1=l1;
    Liste* act2=l2;

    if(l1 == NULL || l2 == NULL) return 0;

    while (act1 != NULL && act2 != NULL) {
        if(strcmp(act1->mot, act2->mot) !=0) {
            if(strcmp(act1->mot, act2->mot) >0) {
                tot += act2->nbr;
                act2 = act2->suivant;
            } else {
                tot += act1->nbr;
                act1 = act1->suivant;
            }
        } else {
            sim += (act1->nbr >= act2->nbr) ? act2->nbr : act1->nbr;
            tot += (act1->nbr >= act2->nbr) ? act2->nbr : act1->nbr;

            act1 = act1->suivant;
            act2 = act2->suivant;
        }
    }

    if(act1 == NULL) {
        while (act2 != NULL) {
            tot += act2->nbr;
            act2 = act2->suivant;
        }
    } else {
        while (act1 != NULL) {
            tot += act1->nbr;
            act1 = act1->suivant;
        }
    }

    return sim/tot;
}


void cleanListe(Liste* l) {
    Liste* act = l;
    Liste* suiv = act;

    while (suiv != NULL) {

        act = suiv;
        suiv=suiv->suivant;

        free(act->mot);
        free(act);
    }
}

void cleanListes(Liste* tab[50]) {
    for (int i = 0; i < 49; i++) {
        if(tab[i] == NULL) return;
        else cleanListe(tab[i]);
    }
}