//
// Created by Leo Peron on 08/12/2021.
//

#include <stdlib.h>
#include "pile.h"

Pile* creerPile() {
    Pile* p = malloc(sizeof(Pile));
    p->tete = NULL;
    p->nb = 0;
    return p;
}

int estVide(Pile* p) {
    return p->nb == 0;
}

void empiler(Pile* p, T_Noeud* n) {
    Cellule* c = malloc(sizeof(Cellule));
    c->n = n;
    c->succ=p->tete;
    p->tete=c;
    p->nb++;
}

T_Noeud* depiler(Pile* p) {
    if(p->tete == NULL) return NULL;
    Cellule *old = p->tete;
    p->tete = p->tete->succ;
    p->nb--;
    T_Noeud* n = old->n;
    old->n = NULL;
    free(old);
    return n;
}
