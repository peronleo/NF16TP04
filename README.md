# NF16 - TP04

## Arbre binaire: Lexique et propriétés

Dans le cadre de l'UV NF16, il était demandé de réalisér un programme pouvant générer x arbre binaire et vérifier certaines propriétés (parfait, équilibré).

Le programme doit également pouvoir ajouter un noeud, en retirer un, afficher l'arbre dans l'ordre lexicographique.

Le programme peut également convertir l'arbre binaire en liste chainée selon l'ordre lexicographique et calculer l'indice de Jacard (similarité du texte) entre deux listes.

### Structures de données utilisées:

- Arbre binaire
- Pile de données
- Liste chainées

### Principes utilisées:

- Récursivité/Itérativité
- Parcours d'arbre Préfixe/Infixe/Postfixe
- Gestion dynamique de la mémoire
