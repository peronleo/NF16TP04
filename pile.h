//
// Created by Leo Peron on 08/12/2021.
//

#ifndef TP04_PILE_H
#define TP04_PILE_H

#include "arbre.h"

typedef struct Cellule {
    T_Noeud* n;
    struct Cellule* succ;
}Cellule;

typedef struct Pile {
    Cellule* tete;
    int nb;
}Pile;

// PILE UTILS

Pile* creerPile();

int estVide(Pile* p);

void empiler(Pile* p, T_Noeud* c);

T_Noeud* depiler(Pile* p);

#endif //TP04_PILE_H
