//
// Created by Leo Peron on 09/12/2021.
//
#include "stdio.h"
#include "utils.h"

int findSpace(Arbre* tab[50]) {
    int i=0;
    while (tab[i]!= NULL && i<49) i++;
    if(i==49) return -1;
    else return i;
}
void affichageAllArbres(Arbre* tab[50]) {
    int i=0;
    while (tab[i] != NULL && i<49) {
        printf("%d. Arbre: %s\n", i, tab[i]->nom);
        i++;
    }
}

void viderBuffer() {
    int c = '0';
    while (c!='\n' && c != EOF) { c = getchar(); }
}